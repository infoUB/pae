## Comentaris generals:
- Hem d'anar molt en compte amb els tipus que utilitzem, sobretot qua nanem fent conversions entre uint8_t, int8_t i int. Passar de uint8_t a int8_t vol dir que els valors més grans de 128 els passa als corresponents negatius (complement a2)

## Coses que he fet:

### dyn_instr.c
- Canviat el "<" de totes les funcions per un "<<", que té molt més sentit considerant l'or bitwise. Així el retorn són dos bits amb 4 estats possibles diferents i possibles.
- Al fer el checksum, s'ha de fer un cast a uint8_t, ja que la operació "~" retorna un signed int.

- implementat el din_write multiposició. Bàsicament utilitza 

### dyn_instr.h
- No m'agrada l'enum dels registres (DYN_REG_t). Els registres són diferents segons si es tracta de motors o sensor, de manera que tindrem elements de l'enum amb valors repetits. Preposo separar els elements per nom tipus ```DYN_REG_SENS__``` i ```DYN_REG_MOTOR__```.

### dyn_app_sensor
-Afegida la funció ```dyn_sensor_left_dist``` per obtenir la distància del sensor esquerre. L'he programada com a funció d'alt nivell (no treballa amb punters de retorn i retorna el valor).

### dyn_emu.c
- Afegint valors hardcoded a la matriu que representa la memòria dels mòduls dynamixel, per fer proves.
