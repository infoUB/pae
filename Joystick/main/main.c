#include <pthread.h>
#include <signal.h>
#include <assert.h>
#include <stdio.h>

#include "main.h"
#include "../dyn/dyn_app_common.h"
#include "../dyn/dyn_app_sensor.h"
#include "../dyn/dyn_app_motors.h"
#include "../dyn_test/dyn_emu.h"
#include "../dyn_test/b_queue.h"
#include "../joystick_emu/joystick.h"


#define LMOTOR_ID 0
#define RMOTOR_ID 1
#define SENSOR_ID 2

uint8_t estado = Ninguno, estado_anterior = Ninguno, finalizar = 0;

/**
 * main.c
 */
int main(void)
{
    pthread_t tid, jid;
    uint8_t tmp;
    int signed_tmp;

    //Init semaphores for TX data handshake
    sem_t sem_a, sem_b;
    sem_tx_msp = &sem_a;
    sem_tx_cli = &sem_b;
    sem_init(sem_tx_msp, 0, 0);
    sem_init(sem_tx_cli, 0, 0);

    //Init queue for RX data
    init_queue();

    //Start thread for dynamixel module emulation
    pthread_create(&tid, NULL, dyn_emu, (void *) &tid);
    pthread_create(&jid, NULL, joystick_emu, (void *) &jid);

    //Testing some high level function
    printf("MAIN: Setting LED to 0 \n");
    dyn_led_control(1, 0);
    printf("MAIN: Getting LED value \n");
    dyn_led_read(1, &tmp);
    assert(tmp == 0);
    printf("MAIN: Setting LED to 1 \n");
    dyn_led_control(1, 1);
    printf("MAIN: Getting LED value \n");
    dyn_led_read(1, &tmp);
    assert(tmp == 1);

    printf("MAIN: Reading sensor distances \n");
    signed_tmp = dyn_sensor_left_dist(2);
    printf("MAIN: Left Distance=%d\n", signed_tmp);
    assert(signed_tmp == 129);
    signed_tmp = dyn_sensor_center_dist(2);
    printf("MAIN: Center Distance=%d\n", signed_tmp);
    assert(signed_tmp == 43);
    signed_tmp = dyn_sensor_right_dist(2);
    printf("MAIN: Right Distance=%d\n", signed_tmp);
    assert(signed_tmp == 214);


    printf("************************\n");
    printf("Test passed successfully\n");
    printf("Pulsar 'q' para terminar, qualquier tecla para seguir\r");
    fflush(stdout);//	return 0;

    while (estado != Quit)
    {
        Get_estado(&estado, &estado_anterior);
        if (estado != estado_anterior)
        {
            Set_estado_anterior(estado);
            printf("estado = %d\n", estado);
            fflush(stdout);
            switch (estado)
            {
                case Up:
                    printf("MAIN: Walk forward\n");
                    move_forward(LMOTOR_ID, RMOTOR_ID,(uint16_t) 0x02FF);
                    printf("\tMAIN: Check LMOTOR Config:");
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__CWLL, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__CWLH, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__CCWLL, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__CCWLH, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__MSL, &tmp);
                    assert(tmp == 0xFF);
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__MSH, &tmp);
                    assert(tmp == 0x02);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__CWLL, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__CWLH, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__CCWLL, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__CCWLH, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__MSL, &tmp);
                    assert(tmp == 0xFF);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__MSH, &tmp);
                    assert(tmp == 0x02);
                    printf("MAIN: Robot moving forward!\n");
                    break;
                case Down:
                    printf("MAIN: Stop Motors\n");
                    stop(LMOTOR_ID, RMOTOR_ID);
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__MSL, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(LMOTOR_ID, DYN_MOTOR_REG__MSH, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__MSL, &tmp);
                    assert(tmp == 0);
                    dyn_read_byte(RMOTOR_ID, DYN_MOTOR_REG__MSH, &tmp);
                    assert(tmp == 0);
                    printf("MAIN: Motors Stopped!");
                    break;
                case Sw1:
                    printf("Boton Sw1 ('a') apretado\n");
                    break;
                case Sw2:
                    printf("Boton Sw2 ('s') apretado\n");
                    break;
                case Quit:
                    printf("Adios!\n");
                    break;
                    //etc, etc...
            }
            fflush(stdout);
        }
    }
    printf("Programa terminado\n");
    //Signal the emulation thread to stop
    pthread_kill(tid, SIGTERM);
    pthread_kill(jid, SIGTERM);
}
