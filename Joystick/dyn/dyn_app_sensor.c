/*
 * dyn_sensor.c
 *
 *  Created on: 18 mar. 2020
 *      Author: droma
 *
 * TODO: High-level functions like "distance_wall_front", etc
 * TODO: Generate another file for motors, with functions like "move_forward", etc
 */
# include "dyn_app_sensor.h"


int dyn_sensor_left_dist(uint8_t id)
{
    uint8_t return_value;
    if (dyn_read_byte(id, DYN_SENS_REG__LDIST, &return_value) == 0)
    {
        return return_value;
    }
    return -1;
}

//Same as distance_Wall_front()
int dyn_sensor_center_dist(uint8_t id)
{
    uint8_t return_value;
    if (dyn_read_byte(id, DYN_SENS_REG__CDIST, &return_value) == 0)
    {
        return return_value;
    }
    return -1;
}

int dyn_sensor_right_dist(uint8_t id)
{
    uint8_t return_value;
    if (dyn_read_byte(id, DYN_SENS_REG__RDIST, &return_value) == 0)
    {
        return return_value;
    }
    return -1;
}





