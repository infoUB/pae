/*
 * dyn_sensor.c
 *
 *  Created on: 18 mar. 2020
 *      Author: droma
 *
 */
# include "dyn_app_sensor.h"

/**
 * Get the value of the left sensor
 * @param id Sensor module ID
 * @return -1 if errors have happened, value otherwise
 */
int dyn_sensor_left_dist(uint8_t id)
{
    uint8_t return_value;
    if (dyn_read_byte(id, DYN_SENS_REG__LDIST, &return_value) == 0)
    {
        return return_value;
    }
    return -1;
}

/**
 * Get the value of the center sensor
 * @param id Sensor module ID
 * @return -1 if errors have happened, value otherwise
 */
int dyn_sensor_center_dist(uint8_t id)
{
    uint8_t return_value;
    if (dyn_read_byte(id, DYN_SENS_REG__CDIST, &return_value) == 0)
    {
        return return_value;
    }
    return -1;
}

/**
 * Get the value of the right sensor
 * @param id Sensor module ID
 * @return -1 if errors have happened, value otherwise
 */
int dyn_sensor_right_dist(uint8_t id)
{
    uint8_t return_value;
    if (dyn_read_byte(id, DYN_SENS_REG__RDIST, &return_value) == 0)
    {
        return return_value;
    }
    return -1;
}





