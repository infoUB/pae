//
// Created by Xavi i Aniol on 01/05/2020.
//

#include "dyn_app_motors.h"

/**
 * Function to set motor to continuous wheel mode
 * @param[in] id
 */
void continuous_rotating_wheel(uint8_t id)
{
    uint8_t zeros[4] = {0,0,0,0}; //Creem un array de 4 0
    dyn_write(id,DYN_MOTOR_REG__CWLL,zeros,4); //Esctivim 4 0's a partir del registre 0x06
}

/**
 * This funcion accepts any kind of movement for 2 wheels
 * @param[in] id1
 * @param[in] id2
 * @param[in] speed1
 * @param[in] speed2
 * @param[in] direction1
 * @param[in] direction2
 */
void move(uint8_t id1, uint8_t id2, uint16_t speed1, uint16_t speed2, bool direction1, bool direction2)
{
    //Ens assegurem que les rodes estiguin en mode continu
    continuous_rotating_wheel(id1);
    continuous_rotating_wheel(id2);

    //Calculem els valors per a cada registre basat en velocitat i direcció:
    uint8_t speed_l1 = speed1 & 0xFF; //Trunquem a 1 byte
    uint8_t speed_h1 = ((direction1 << 2) & 0x04) | ((speed1 >> 8) & 0x03);
    uint8_t speed_l2 = speed2 & 0xFF;
    uint8_t speed_h2 = ((direction2 << 2) & 0x04) | ((speed2 >> 8) & 0x03);

    //Escrivim als registres de cada motor els valors que hem calculat
    dyn_write_byte(id1,DYN_MOTOR_REG__MSL,speed_l1);
    dyn_write_byte(id1,DYN_MOTOR_REG__MSH,speed_h1);
    dyn_write_byte(id2,DYN_MOTOR_REG__MSL,speed_l2);
    dyn_write_byte(id2,DYN_MOTOR_REG__MSH,speed_h2);
}

/**
 * This function makes the robot go forward indefinitely
 */
void move_forward(uint8_t id1, uint8_t id2, uint16_t speed)
{
    move(id1,id2,speed,speed,0,0);
}

/**
 * This function makes the robot go backwards indefinitely
 */
void move_backwards(uint8_t id1, uint8_t id2, uint16_t speed)
{
    move(id1,id2,speed,speed,1,1);
}

/**
 * Spin left or right
 *
 * @param id1 id of the left wheel
 * @param id2 id of the right wheel
 * @param speed speed of the rotation
 * @param direction 1 for rotating clockwise, 0 for counterclockwise
 */
void spin(uint8_t id1, uint8_t id2, uint16_t speed, bool direction)
{
    uint16_t half_speed = speed >> 1; //Reduim la velocitat a la meitat doncs cada roda anira en direcció oposada
    move(id1, id2, half_speed, half_speed,!direction, direction);
}

void spin_left(uint8_t id1, uint8_t id2, uint16_t speed)
{
    spin(id1,id2,speed,0);
}

void spin_right(uint8_t id1, uint8_t id2, uint16_t speed)
{
    spin(id1,id2,speed,1);
}

/**
 * This funcition will change the speed without changing the direction or rotation
 * the wheels are going, so the turning angle is conserved
 * @param id1 Left wheel ID
 * @param id2 Right wheel ID
 * @param speed
 */
void change_speed(uint8_t id1, uint8_t id2, uint16_t speed)
{
    //Determinem quina roda es la mes rapida per actualitzarla al nou valor

    //En primer lloc treiem velocitats acutals
    uint8_t speed_l1;
    uint8_t speed_h1;
    uint8_t speed_l2;
    uint8_t speed_h2;
    dyn_read_byte(id1,DYN_MOTOR_REG__MSL,&speed_l1);
    dyn_read_byte(id1,DYN_MOTOR_REG__MSH,&speed_h1);
    dyn_read_byte(id2,DYN_MOTOR_REG__MSL,&speed_l2);
    dyn_read_byte(id2,DYN_MOTOR_REG__MSH,&speed_h2);

    bool direction1 = (speed_h1 & 0x03 == 0x03); //Extreiem les direccions
    bool direction2 = (speed_h2 & 0x03 == 0x03);
    uint16_t speed1 = ((speed_h1 << 8) | speed_l1) & 0x3FF; //Treiem únicament la velocitat sense direccions ni res mes
    uint16_t speed2 = ((speed_h2 << 8) | speed_l2) & 0x3FF;

    //Fem les regles de 3 per conservar la proporció de velocitats
    if(speed1 > speed2)
    {
        speed2 = (speed2*speed)/speed1;
        speed1 = speed;
    }
    else
    {
        speed1 = (speed1*speed)/speed2;
        speed2 = speed;
    }

    move(id1,id2,speed1,speed2,direction1,direction2);
}

/**
 * Makes motors stop
 * @param id1 Left wheel ID
 * @param id2 Right wheel ID
 */
void stop(uint8_t id1, uint8_t id2)
{
    move(id1,id2,0x0,0x0,1,1);
}