#include <pthread.h>
#include <signal.h>
#include <assert.h>
#include <posicion.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>

#include "main.h"
#include "dyn/dyn_app_common.h"
#include "dyn/dyn_app_motors.h"
#include "dyn/dyn_app_sensor.h"
#include "dyn_test/dyn_emu.h"
#include "dyn_test/b_queue.h"
#include "joystick_emu/joystick.h"
#include "habitacion_001.h"


#define ORIENTATION_TH 1    //Tolerància màxima de la distància al orientar
#define SENSOR_TH 150       //Distància màxima per detectar un gor obert
#define SPEED 767           //Velocitat en condicions normals
#define TURNING_SPEED 814   //Velocitat del segon motor en girar
#define ROT_SPEED 300       //Velocitat en spin
#define MIN_WALL_DIST 3     //Distància màxima a la que ens volem acostar a la paret
#define MAX_WALL_DIST 9     //Distància màxima a la que ens volem allunyar de la paret
#define MEDIUM_WALL_DIST 6  //Distància òptima a la paret
#define DERIVATIVE_MULTIPLIER 32 //Multiplicador de la derivada de l'error
#define ERROR_MULTIPLIER 3  //Multiplicador de l'error
#define LEFT_MOTOR_ID 1
#define RIGHT_MOTOR_ID 2
#define SENSOR_ID 3

//Definició de funció debug per fer prints.
// Descomentant la primera línia i coemntant la segona debug passa a ser un àlies de printf.
// Per defecte debug es defineix com a void i no imprimeix res. Això permet activar o desectivar
//      els missatge de debug molt ràpidament
#ifndef __DEBUG__
#define __DEBUG__
//#define debug(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#define debug(fmt, ...) ((void)0)
#endif

uint32_t indice;

/**
 * Funció per calcular la distància del robot a la paret
 * @return int
 */
int calculate_wall_dist()
{
    int x = dyn_sensor_left_dist(SENSOR_ID);
    int y = dyn_sensor_center_dist(SENSOR_ID);
    double result = (x*y)/sqrt(pow(x,2) + pow(y,2));
    debug("x: %d \t y: %d\t z: %d\t dist: %d\n", x,y,(int)sqrt(pow(x,2) + pow(y,2)),(x*y)/(int)sqrt(pow(x,2) + pow(y,2)));
    return (int)round(result);
}

/**
 * Rota en la direcció indicada fins a trobar un mínim en la distància lateral esquerra
 * @param direction
 */
void find_minimum(bool direction)
{
    if(direction)
    {
        spin_left(LEFT_MOTOR_ID, RIGHT_MOTOR_ID,ROT_SPEED);
    }
    else
    {
        spin_right(LEFT_MOTOR_ID, RIGHT_MOTOR_ID, ROT_SPEED);
    }

    int x;
    int prev_x = dyn_sensor_left_dist(SENSOR_ID);
    while (!simulator_finished)
    {
        x = dyn_sensor_left_dist(SENSOR_ID);
        if (prev_x < x)
        {
            break;
        }
        prev_x = x;
    }
    stop(1,2);
    return;
}

/**
 * main.c
 */
int main(void) {
    pthread_t tid, jid;

    //Init queue for TX/RX dataswitch(wall_dist)
    init_queue(&q_tx);
    init_queue(&q_rx);

    //Start thread for dynamixel module emulation
    // Passing the room information to the dyn_emu thread
    pthread_create(&tid, NULL, dyn_emu, (void *) datos_habitacion);
    pthread_create(&jid, NULL, joystick_emu, (void *) &jid);


    int left_dist, right_dist, center_dist, wall_dist;
    int error, current_error, change, derivative;
    int last_error = 0;
    int x, prev_x;

    bool nearest_wall_found = false;

    while(!simulator_finished)
    {
        /* PRIMERA PART: TROBAR PARET MÉS PROPERA */
        while(!nearest_wall_found)
        {
            //Fem lectures de tots els sensors
            left_dist = dyn_sensor_left_dist(SENSOR_ID);
            right_dist = dyn_sensor_right_dist(SENSOR_ID);
            center_dist = dyn_sensor_center_dist(SENSOR_ID);

            //Si el sensor esquerre detecta la distància mínima
            if(left_dist < center_dist && left_dist < right_dist)
            {
                debug("Roto a la esquerra\n");
                //Gira fins que el sensor central detecti aquesta mateixa distància
                spin_left(LEFT_MOTOR_ID, RIGHT_MOTOR_ID, ROT_SPEED);
                while(dyn_sensor_center_dist(SENSOR_ID)-left_dist >= ORIENTATION_TH);
                stop(LEFT_MOTOR_ID, RIGHT_MOTOR_ID);

            }
            //Si el sensor dret detecta la distància mínima
            else if(right_dist < center_dist && right_dist < left_dist)
            {
                debug("Roto a la dreta\n");
                //Gira fins que el sensor central detecti aquesta mateixa distància
                spin_right(LEFT_MOTOR_ID, RIGHT_MOTOR_ID, ROT_SPEED);
                while(dyn_sensor_center_dist(SENSOR_ID)-right_dist >= ORIENTATION_TH);
                stop(LEFT_MOTOR_ID, RIGHT_MOTOR_ID);
            }
            //En cas contrari el sensor central és el mínim
            else
            {
                //Movem fins que tinguem la paret al davant a la distància mitjana
                move_forward(LEFT_MOTOR_ID, RIGHT_MOTOR_ID,SPEED);
                if(center_dist <= MAX_WALL_DIST)
                {
                    debug("Ja he arribat\n");
                    nearest_wall_found = true; //Posem l'estat de cerca paret a true
                    //Girem a la dreta fins que la paret que hem trobat ens quedi a l'esquerra
                    spin_right(LEFT_MOTOR_ID, RIGHT_MOTOR_ID,ROT_SPEED);
                    while(abs(dyn_sensor_left_dist(SENSOR_ID) - center_dist) >= ORIENTATION_TH);
                    stop(LEFT_MOTOR_ID, RIGHT_MOTOR_ID);
                    debug("Esquerra: %d\t Centre previ: %d\n", dyn_sensor_left_dist(SENSOR_ID),center_dist);
                }
            }
        }

        //debug("Distància: %d\n", calculate_wall_dist());

        /* PART SEGONA: SEGUIR LA PARET */
        wall_dist = calculate_wall_dist();
        if(wall_dist <= MIN_WALL_DIST)
        {
            find_minimum(0);
        }
        else if(wall_dist >= MAX_WALL_DIST)
        {

            x = dyn_sensor_left_dist(SENSOR_ID);
            prev_x = x;
            //Si el sensor esquerre ha perdut la paret
            if (x > SENSOR_TH)
            {
                //Rotem a l'esquerra fins que la distància a l'esquerra hagi canviat dos cops
                spin_left(LEFT_MOTOR_ID, RIGHT_MOTOR_ID,ROT_SPEED);
                debug("ESTAT ESPECIAL !!!!!!!!!!!!!!!!!!!!!");
                bool reduced_twice = false;
                while (!simulator_finished)
                {
                    x = dyn_sensor_left_dist(SENSOR_ID);
                    if(x != prev_x)
                    {
                        if(reduced_twice)
                        {
                            break;
                        }
                        reduced_twice = true;
                        prev_x = x;
                    }
                }
                debug("Previa x: %d \t X actual: %d\n", prev_x, x);
                stop(1,2);
            }
            else //Si només ens hem allunyat
            {
                debug("Allunyant-se molt de la pared \t Distancia:%d \t Sensor esquerra: %d Sensor frontal: %d\n",
                      wall_dist, dyn_sensor_left_dist(SENSOR_ID), dyn_sensor_center_dist(SENSOR_ID));
                //Rota a l'esquerra fins que el sensor esquerre detecti un mínim en la distància
                find_minimum(1);
            }
        }

        //Segueix la paret compensant l'error
        wall_dist = calculate_wall_dist();
        current_error = MEDIUM_WALL_DIST - wall_dist;
        derivative = current_error - last_error;
        change = ERROR_MULTIPLIER*current_error + DERIVATIVE_MULTIPLIER*derivative;
        //debug("Error: %d, %d, %d\n", current_error, SPEED + change, SPEED - change);
        move(1, 2, SPEED + change, SPEED - change, 0, 0);
        last_error = current_error;
    }

    debug("Ja estic!!!\n");
    //Signal the emulation thread to stop
    pthread_kill(tid, SIGTERM);
    pthread_kill(jid, SIGTERM);
    printf("Programa terminado\n");
    fflush(stdout);
    exit(0);
}


