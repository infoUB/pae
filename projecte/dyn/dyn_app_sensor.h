/*
 * dyn_sensor.h
 *
 *  Created on: 18 mar. 2020
 *      Author: droma
 */

#ifndef DYN_SENSOR_H_
#define DYN_SENSOR_H_

#include <stdint.h>
#include "dyn_instr.h"

int dyn_sensor_left_dist(uint8_t id);

int dyn_sensor_center_dist(uint8_t id);

int dyn_sensor_right_dist(uint8_t id);


#endif /* DYN_SENSOR_H_ */
