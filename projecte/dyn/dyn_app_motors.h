//
// Created by Xavi on 03/05/2020.
//

#include <stdint.h>
#include <stdbool.h>
#include "dyn_instr.h"

#ifndef JOYSTICK_DYN_APP_MOTORS_H
#define JOYSTICK_DYN_APP_MOTORS_H

#endif //JOYSTICK_DYN_APP_MOTORS_H

void continuous_rotating_wheel(uint8_t id);

void move(uint8_t id1, uint8_t id2, uint16_t speed1, uint16_t speed2, bool direction1, bool direction2);

void move_forward(uint8_t id1, uint8_t id2, uint16_t speed);

void move_backwards(uint8_t id1, uint8_t id2, uint16_t speed);

void spin(uint8_t id1, uint8_t id2, uint16_t speed, bool direction);

void spin_left(uint8_t id1, uint8_t id2, uint16_t speed);

void spin_right(uint8_t id1, uint8_t id2, uint16_t speed);

void stop(uint8_t id1, uint8_t id2);




