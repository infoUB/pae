#include <pthread.h>
#include <signal.h>
#include <assert.h>
#include <posicion.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>

#include "main.h"
#include "dyn/dyn_app_common.h"
#include "dyn/dyn_app_motors.h"
#include "dyn/dyn_app_sensor.h"
#include "dyn_test/dyn_emu.h"
#include "dyn_test/b_queue.h"
#include "joystick_emu/joystick.h"
#include "habitacion_001.h"

#define EXPECTED_WALL_DIST 20
#define PARALLEL_TH 1
#define ORIENTATION_TH 1
#define SPEED 767
#define ROT_SPEED 300
#define MIN_WALL_DIST 3
#define MAX_WALL_DIST 9
#define MEDIUM_WALL_DIST 6
#define CORRECTION_SPEED 0
#define MUL_CORRECTION_SPEED 20

#ifndef __DEBUG__
#define __DEBUG__
#define debug(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
//#define debug(fmt, ...) ((void)0)

#endif

uint8_t estado = Ninguno, estado_anterior = Ninguno, finalizar = 0;
uint32_t indice;

int calculate_wall_dist()
{
    int x = dyn_sensor_left_dist(3);
    int y = dyn_sensor_center_dist(3);
    double result = (x*y)/sqrt(pow(x,2) + pow(y,2));
    //debug("x: %d \t y: %d\t z: %d\t dist: %d\n", x,y,(int)sqrt(pow(x,2) + pow(y,2)),(x*y)/(int)sqrt(pow(x,2) + pow(y,2)));
    return (int)round(result);
}

/**
 * main.c
 */
int main(void) {
    pthread_t tid, jid;
    uint8_t tmp;

    //Init queue for TX/RX dataswitch(wall_dist)
    init_queue(&q_tx);
    init_queue(&q_rx);

    //Start thread for dynamixel module emulation
    // Passing the room information to the dyn_emu thread
    pthread_create(&tid, NULL, dyn_emu, (void *) datos_habitacion);
    pthread_create(&jid, NULL, joystick_emu, (void *) &jid);

    //Testing some high level function
    printf("\nSetting LED to 0 \n");
    dyn_led_control(1, 0);
    printf("\nGetting LED value \n");
    dyn_led_read(1, &tmp);
    assert(tmp == 0);
    printf("\nSetting LED to 1 \n");
    dyn_led_control(1, 1);
    printf("\nGetting LED value \n");
    dyn_led_read(1, &tmp);
    assert(tmp == 1);
    int signed_tmp = dyn_sensor_left_dist(3);
    printf("MAIN: Left Distance=%d\n", signed_tmp);
    signed_tmp = dyn_sensor_right_dist(3);
    printf("MAIN: Right Distance=%d\n", signed_tmp);
    signed_tmp = dyn_sensor_right_dist(3);
    printf("MAIN: Center Distance=%d\n", signed_tmp);

    printf("\n************************\n");
    printf("Test passed successfully\n");

    printf("\nDimensiones habitacion %d ancho x %d largo mm2\n", ANCHO, LARGO);
    printf("En memoria: %I64u B = %I64u MiB\n", sizeof(datos_habitacion), sizeof(datos_habitacion) >> 20);

    printf("Pulsar 'q' para terminar, qualquier tecla para seguir\n");
    fflush(stdout);//	return 0;

    /*while (estado != Quit) {
        if (simulator_finished) {
            break;
        }
        Get_estado(&estado, &estado_anterior);
        if (estado != estado_anterior) {
            Set_estado_anterior(estado);
            printf("estado = %d\n", estado);
            switch (estado) {
                case Sw1:
                    printf("Boton Sw1 ('a') apretado\n");
                    dyn_led_control(1, 1); //Probaremos de encender el led del motor 2
                    printf("\n");
                    break;
                case Sw2:
                    printf("Boton Sw2 ('s') apretado\n");
                    dyn_led_control(1, 0); //Probaremos de apagar el led del motor 2
                    printf("\n");
                    break;
                case Up:

                    break;
                case Down:

                    break;
                case Left:
                    //Comprobaremos si detectamos las esquinas de la pared izquierda:
                    printf("Esquina inferior izquierda:\n");
                    printf("(1, 1): %d (fuera pared)\n", obstaculo(1, 1, datos_habitacion));
                    printf("(0, 1): %d (pared izq.)\n", obstaculo(0, 1, datos_habitacion));
                    printf("(1, 0): %d (pared del.)\n", obstaculo(1, 0, datos_habitacion));
                    printf("(0, 0): %d (esquina)\n", obstaculo(0, 0, datos_habitacion));
                    printf("Esquina superior izquierda:\n");
                    printf("(1, 4094): %d (fuera pared)\n", obstaculo(1, 4094, datos_habitacion));
                    printf("(0, 4094): %d (pared izq.)\n", obstaculo(0, 4094, datos_habitacion));
                    printf("(1, 4095): %d (pared fondo.)\n", obstaculo(1, 4095, datos_habitacion));
                    printf("(0, 4095): %d (esquina)\n", obstaculo(0, 4095, datos_habitacion));
                    break;
                case Right:
                    //Comprobaremos si detectamos las esquinas de la pared derecha:
                    printf("Esquina inferior derecha:\n");
                    printf("(4094, 1): %d (fuera pared)\n", obstaculo(4094, 1, datos_habitacion));
                    printf("(4094, 0): %d (pared del.)\n", obstaculo(4094, 0, datos_habitacion));
                    printf("(4095, 1): %d (pared der.)\n", obstaculo(4095, 1, datos_habitacion));
                    printf("(4095, 0): %d (esquina)\n", obstaculo(4095, 0, datos_habitacion));
                    printf("Esquina superior derecha:\n");
                    printf("(4094, 4094): %d (fuera pared)\n", obstaculo(4094, 4094, datos_habitacion));
                    printf("(4094, 4095): %d (pared fondo)\n", obstaculo(4094, 4095, datos_habitacion));
                    printf("(4095, 4094): %d (pared der.)\n", obstaculo(4095, 4094, datos_habitacion));
                    printf("(4095, 4095): %d (esquina)\n", obstaculo(4095, 4095, datos_habitacion));
                    break;
                case Center:

                    break;
                case Quit:
                    printf("Adios!\n");
                    break;
                    //etc, etc...
            }
            fflush(stdout);
        }
        move_forward(1,2,(uint16_t) 0x02FF);
        signed_tmp = dyn_sensor_center_dist(3);
        printf("MAIN: Center Distance=%d\n", signed_tmp);
        while(signed_tmp > 100){
            signed_tmp = dyn_sensor_center_dist(3);
            printf("MAIN: Center Distance=%d\n", signed_tmp);
        }
        move_forward(1,2,(uint16_t) 0);


    }*/
    int left_dist;
    int right_dist;
    int center_dist;
    int wall_dist;
    bool nearest_wall_found = false;

    while(!simulator_finished)
    {
        //Trobar pared propera
        while(!nearest_wall_found)
        {
            left_dist = dyn_sensor_left_dist(3);
            right_dist = dyn_sensor_right_dist(3);
            center_dist = dyn_sensor_center_dist(3);

            if(left_dist < center_dist && left_dist < right_dist)
            {
                debug("Roto a la esquerra\n");
                spin_left(1,2, ROT_SPEED);
                while(dyn_sensor_center_dist(3)-left_dist >= ORIENTATION_TH);
                stop(1,2);

            }
            else if(right_dist < center_dist && right_dist < left_dist)
            {
                debug("Roto a la dreta\n");

                spin_right(1,2, ROT_SPEED);
                while(dyn_sensor_center_dist(3)-right_dist >= ORIENTATION_TH);
                stop(1,2);
            }
            else
            {
                move_forward(1,2,SPEED);
                if(center_dist <= MAX_WALL_DIST)
                {
                    debug("Ja he arribat\n");
                    nearest_wall_found = true;
                    //Ens orientem per tenir la pared a l'esquerra
                    spin_right(1,2,ROT_SPEED);
                    while(abs(dyn_sensor_left_dist(3) - center_dist) >= ORIENTATION_TH);
                    stop(1,2);
                    debug("Esquerra: %d\t Centre previ: %d\n", dyn_sensor_left_dist(3),center_dist);
                }
            }
        }

        debug("Distància: %d\n", calculate_wall_dist());
        //Hem trobat la paret que volem seguir i estem paral·lels a ella
        int prev_wall_dist = calculate_wall_dist();
        move_forward(1,2,SPEED);
        int speedcorrect = 820;
        while(!simulator_finished)
        {
            wall_dist = calculate_wall_dist();
/*            if(dyn_sensor_center_dist(3)<=MIN_WALL_DIST || dyn_sensor_right_dist(3)<=MIN_WALL_DIST)
            {
                debug("Possible colisio, refent trajectoria\n");
                spin_right(1, 2, ROT_SPEED);
                //usleep(5000);
                //while(abs(dyn_sensor_left_dist(3)-calculate_wall_dist()) > PARALLEL_TH);
                int x;
                int prev_x = dyn_sensor_left_dist(3);
                while (!simulator_finished)
                {
                    x = dyn_sensor_left_dist(3);
                    if (prev_x < x)
                    {
                        break;
                    }
                    prev_x = x;
                }
                move(1,2,SPEED,SPEED,0,0);
            }*/
            //Per si arriba als limits, spin per no pasar-hi mai
            if (wall_dist <= MIN_WALL_DIST)
            {
                debug("Apropant-se molt a la pared \t Distancia:%d\n",wall_dist);
                spin_right(1, 2, ROT_SPEED);
                //usleep(5000);
                //while(abs(dyn_sensor_left_dist(3)-calculate_wall_dist()) > PARALLEL_TH);
                int x;
                int prev_x = dyn_sensor_left_dist(3);
                while (!simulator_finished)
                {
                    x = dyn_sensor_left_dist(3);
                    if (prev_x < x)
                    {
                        break;
                    }
                    prev_x = x;
                }
                move_forward(1,2,SPEED);
            }
            else if (wall_dist >= MAX_WALL_DIST)
            {
                if(dyn_sensor_left_dist(3) > 150)
                {
                    spin_left(1,2,ROT_SPEED);
                    while(dyn_sensor_left_dist(3)>=MAX_WALL_DIST);
                    move(1,2,SPEED,850,0,0);
                    while(calculate_wall_dist() >= MIN_WALL_DIST);

                }
                else
                {
                    debug("Allunyant-se molt de la pared \t Distancia:%d \t Sensor esquerra: %d Sensor frontal: %d\n",
                          wall_dist, dyn_sensor_left_dist(3), dyn_sensor_center_dist(3));
                    spin_left(1, 2, ROT_SPEED);
                    //usleep(5000);
                    //while(abs(dyn_sensor_left_dist(3)-calculate_wall_dist()) > PARALLEL_TH);
                    int x;
                    int prev_x = dyn_sensor_left_dist(3);
                    while (!simulator_finished)
                    {
                        x = dyn_sensor_left_dist(3);
                        if (prev_x < x)
                        {
                            break;
                        }
                        prev_x = x;
                    }
                    if (x > 150)
                    {
                        debug("ESTAT ESPECIAL !!!!!!!!!!!!!!!!!!!!!");
                        while (dyn_sensor_center_dist(3) > 150);
                        move_forward(1, 2, SPEED);
                        while (calculate_wall_dist() > MEDIUM_WALL_DIST);
                    }
                    debug("Previa x: %d \t X actual: %d\n", prev_x, x);
                }
                //move(1,2,SPEED,820,0,0);
                move_forward(1,2,SPEED);
            }

            else if (prev_wall_dist != wall_dist)
            {
                if(prev_wall_dist < wall_dist) //gira esquerra
                {
                    move(1,2,SPEED,800,0,0);
                }
                else
                {
                    move(1,2,800,SPEED,0,0);
                }
/*                if (wall_dist < MEDIUM_WALL_DIST)
                {
                    move(1, 2, SPEED,
                         SPEED * (1 + CORRECTION_SPEED - (MEDIUM_WALL_DIST - wall_dist) / (MUL_CORRECTION_SPEED*MEDIUM_WALL_DIST)), 0, 0);
                } else if (wall_dist > MEDIUM_WALL_DIST)
                {
                    move(1, 2, SPEED * (1 + CORRECTION_SPEED - (wall_dist - MEDIUM_WALL_DIST) / (MUL_CORRECTION_SPEED*MEDIUM_WALL_DIST)),
                         SPEED, 0, 0);
                } else
                {
                    move_forward(1, 2, SPEED);
                }*/
            }
            prev_wall_dist = wall_dist;
        }
    }


    debug("Ja estic!!!\n");
    //Signal the emulation thread to stop
    pthread_kill(tid, SIGTERM);
    pthread_kill(jid, SIGTERM);
    printf("Programa terminado\n");
    fflush(stdout);
    exit(0);
}


