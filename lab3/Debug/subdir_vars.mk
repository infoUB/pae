################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

LIB_SRCS += \
../lib_PAE2.lib 

C_SRCS += \
../Practica3.c \
../startup_msp432p401r_ccs.c \
../system_msp432p401r.c 

C_DEPS += \
./Practica3.d \
./startup_msp432p401r_ccs.d \
./system_msp432p401r.d 

OBJS += \
./Practica3.obj \
./startup_msp432p401r_ccs.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"Practica3.obj" \
"startup_msp432p401r_ccs.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"Practica3.d" \
"startup_msp432p401r_ccs.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../Practica3.c" \
"../startup_msp432p401r_ccs.c" \
"../system_msp432p401r.c" 


